<?php

require_once( "Slim/Slim.php" );
require_once( "server/config/Config.php" );
require_once( "server/config/Database.php" );
require_once( "server/config/Response.php" );
require_once( "server/models/Catalog.php" );
require_once( "server/models/User.php" );
require_once( "server/config/Login.php" );

\Slim\Slim::registerAutoloader();

$db      = new Database();
$Catalog = new Catalog( $db->dbh );
$Login   = new Login( $db->dbh );
$User    = new User( $db->dbh );

$app = new \Slim\Slim( array(
	'templates.path' => TEMPLATE_DIR
) );

$app->get( "/", function () use ( $app ) {
	$app->render( "index.php" );
} );

$app->get( "/:param", function () use ( $app ) {
	$app->render( "index.php" );
} );

/*      === LOG IN METHODS ===       */

$app->post( "/api/login", function () use ( $app, $Login ) {
	$result = (array) json_decode( $app->request->getBody() );
	if ( isset( $result["login"] ) && isset( $result["password"] ) ) {
		if ( $result = $Login->SignIn( $result["login"], $result["password"] ) ) {
			Response::Send( [ "success" => true, "user" => $result ], "json" );
		}
	}
	Response::Send( [ "success" => false ], "json" );
} );

$app->get( "/api/logout", function () use ( $db, $Login ) {
	$Login->SignOut();
	Response::SendStatus( 200 );
} );

/*      === REQUESTS METHODS ===       */

$app->get( "/api/requests/:params", function () use ( $app, $db ) {
	$params = $app->request()->get();
	$format = $app->request()->get( "format" );
	Response::Send( $db->get_requests( $params ), $format );
} );

$app->post( "/api/requests/:params", function () use ( $app, $db ) {
	Response::Send( [ "success" => $db->add_request( (array) json_decode( $app->request->getBody() ) ) ], "json" );
} );


$app->put( "/api/requests/:params", function () use ( $app, $db ) {
	if ( $app->request()->put() ) {
		$data = $app->request()->put();
	} else {
		$data = (array) json_decode( $app->request->getBody() );
	}
	$format = $app->request()->get( "format" );
	Response::Send( [ "success" => $db->update_request( $data ) ], $format );
} );

/*      === CATALOGS METHODS ===       */

$app->get( "/api/get_catalog_list", function () use ( $Catalog ) {
	Response::Send( $Catalog->get_catalogs(), "json" );
} );

$app->get( "/api/catalog_fields/:catalog_name", function ( $catalog_name ) use ( $Catalog ) {
	Response::Send( $Catalog->get_catalog_fields( $catalog_name ), "json" );
} );

$app->get( "/api/catalog/:catalog_name", function ( $catalog_name ) use ( $Catalog ) {
	Response::Send( $Catalog->get_catalog_data( $catalog_name ), "json" );
} );

$app->delete( "/api/catalog/:catalog_name/:id", function ( $catalog_name, $id ) use ( $Catalog ) {
	Response::Send( [ "success" => $Catalog->delete_row_from_catalog( $catalog_name, $id ) ], "json" );
} );

$app->put( "/api/catalog/:catalog_name/:id", function ( $catalog_name, $id ) use ( $app, $Catalog ) {
	Response::Send( [ "success" => $Catalog->update_catalog_row( $catalog_name, $id, (array) json_decode( $app->request->getBody() ) ) ], "json" );
} );

$app->post( "/api/catalog/:catalog_name", function ( $catalog_name ) use ( $app, $Catalog ) {
	Response::Send( [ "success" => $Catalog->insert_catalog_row( $catalog_name, (array) json_decode( $app->request->getBody() ) ) ], "json" );
} );


/*      === MISC METHODS ===       */

$app->get( "/api/catalog/get_locations/:region_id/:client_id", function ( $region_id, $client_id ) use ( $app, $Catalog ) {
	Response::Send( $Catalog->get_locations_by_region( $region_id, $client_id ), "json" );
} );

$app->get( "/api/catalog/get_clients_by_region/:region_id", function ( $region_id ) use ( $app, $Catalog ) {
	Response::Send( $Catalog->get_clients_by_region( $region_id ), "json" );
} );


/*      === USER REQUESTS METHODS ===       */
$app->get( "/api/user/:params", function () use ( $app, $User ) {
	$params = $app->request()->get();
	$format = $app->request()->get( "format" );
	Response::Send( $User->get_users( $params ), $format );
} );


/*      === MOBILE REQUESTS METHODS ===       */

$app->post( "/api/mobile_login", function () use ( $app, $Login ) {
	$result = $app->request->post();
	if ( isset( $result["login"] ) && isset( $result["password"] ) ) {
		if ( $result = $Login->SignIn( $result["login"], $result["password"] ) ) {
			Response::Send( $result, "xml" );
		}
	}
	Response::SendText( "false" );
} );

$app->post( "/api/take_request/:id", function ( $id ) use ( $db, $app ) {
	Response::Send( [ "success" => $db->android_take_request( $id, $app->request->post() ) ], "xml" );
} );

//TODO: сделать общим api для web и для mobile
$app->get( "/mobile_api/catalog/:catalog_name", function ( $catalog_name ) use ( $Catalog ) {
	Response::Send( $Catalog->get_catalog_data( $catalog_name ), "xml" );
} );

/*      === NOT DEFINED REQUESTS METHODS ===       */

$app->notFound( function () use ( $app ) {
	Response::Send404();
} );


$app->run();