var app = angular.module("app", ["ngResource", "ngRoute"]);
app.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider
        .when("/", {templateUrl: "/public/app/partials/main/main.html", controller: "mainCtrl"})
        .when("/request_create", {templateUrl: "/public/app/partials/requests/create_request.html", controller: "createRequestCtrl"})
        .when("/request_list", {templateUrl: "/public/app/partials/requests/request_list.html", controller: "requestListCtrl"})
        .when("/catalog", {templateUrl: "/public/app/partials/catalog/catalog_list.html", controller: "catalogListCtrl"})
        .otherwise({redirectTo: '/'});
});