app.controller("catalogListCtrl", function ($scope, $location, CatalogList, Catalog, CatalogFields, Identity, $q) {

        $scope.identity = Identity;
        if (Identity.currentUser == null) {
            $location.path("/");
            return;
        }

        window.myscope = $scope;

        $scope.catalog_title = "";
        $scope.dependencies = {};

        CatalogList.query().$promise.then(function (collection) {
            $scope.catalogs = collection[0];
        });

        $scope.temp_row = {};
        $scope.temp_add_row = {};

        $scope.showCatalogData = function (catalog_name, catalog_title) {

            $scope.activeValue = catalog_name;
            $(".new_catalog_row").addClass("hidden");
            $scope.temp_row = {};
            $scope.temp_add_row = {};

            $scope.catalog_title = catalog_title;
            $scope.catalog_name = catalog_name;
            $scope.catalog_data = Catalog.query({catalog_name: catalog_name});
            $scope.catalog_fields = CatalogFields.query({catalog_name: catalog_name});

            var dfd = $q.defer();

            $scope.dependencies[catalog_name] = {};
            var dependencies = $scope.catalogs[catalog_name].dependencies;
            _.keys(dependencies).forEach(function (dep_cat_name, index) {
                Catalog.query({catalog_name: dep_cat_name}).$promise.then(function (collection) {
                    $scope.dependencies[catalog_name][dependencies[dep_cat_name]] = _.indexBy(collection, "id");
                    if (index == _.keys(dependencies).length - 1) dfd.resolve();
                });
            });
        };

        $scope.editRow = function (row_id, key_id) {
            $scope.temp_row = _.clone($scope.catalog_data[key_id]);

            $("#catalog_data_row_" + row_id + " td").each(function () {
                var data_attribute = $(this).attr("data");
                if (data_attribute != null && data_attribute != "id") {
                    var data_container = $(this).find("span.data_container").first(),
                        data_input = $(this).find(".data_input").first();

                    data_container.addClass("hidden");
                    data_input.removeClass("hidden");

                }
                if ($(this).hasClass("actions")) {
                    $(this).find("button").each(function () {
                        var data_attribute = $(this).attr("data");
                        if (["edit_btn", "delete_btn"].indexOf(data_attribute) != -1) {
                            $(this).addClass("hidden");
                        } else {
                            $(this).removeClass("hidden");
                        }
                    });
                }
            });
        };

        $scope.cancelAmend = function (row_id, key_id) {
            $scope.catalog_data[key_id] = $scope.temp_row;

            $("#catalog_data_row_" + row_id + " td").each(function () {
                var data_attribute = $(this).attr("data");
                if (data_attribute != null && data_attribute != "id") {
                    var data_container = $(this).find("span.data_container").first(),
                        data_input = $(this).find(".data_input").first();

                    if (data_attribute != "password") data_container.removeClass("hidden");
                    data_input.addClass("hidden");
                }
                if ($(this).hasClass("actions")) {
                    $(this).find("button").each(function () {
                        data_attribute = $(this).attr("data");
                        if (["edit_btn", "delete_btn"].indexOf(data_attribute) != -1) {
                            $(this).removeClass("hidden");
                        } else {
                            $(this).addClass("hidden");
                        }
                    });
                }
            });
        };

        $scope.deleteRecord = function (row_id) {
            bootbox.confirm("Запись будет удалена безвозвратно, вы уверены?", function (confirmed) {
                if (confirmed)
                    Catalog.delete({catalog_name: $scope.catalog_name, id: row_id}).$promise.then(function (response) {
                        if (response.success) {
                            toastr.success("Запись успешно удалена!");
                            $("#catalog_data_row_" + row_id).remove();
                        } else {
                            toastr.error("Произошла ошибка при удалении, обратитесь к администратору, возможно у вас нет прав!");
                        }
                    });
            });
        };

        $scope.applyAmend = function (row_id, key_id) {
            var data = _.clone($scope.catalog_data[key_id]);
            delete data.id;
            delete data.$$hashKey;
            for (var key in data) {
                if (!data.hasOwnProperty(key)) continue;
                if (!data[key]) {
                    toastr.error("Пустое поле недопустимо!");
                    return;
                }
            }

            Catalog.update({catalog_name: $scope.catalog_name, id: row_id}, data).$promise.then(function (response) {
                if (response.success) {
                    toastr.success("Запись успешно изменена!");
                    $scope.cancelAmend(row_id);
                } else {
                    toastr.error("Произошла ошибка при изменении, обратитесь к администратору, возможно у вас нет прав!");
                }
            });
        };

        $scope.addRecord = function () {
            $(".new_catalog_row").removeClass("hidden");
        };

        $scope.applyAdding = function () {
            Catalog.save({catalog_name: $scope.catalog_name}, $scope.temp_add_row).$promise.then(function (response) {
                if (response.success) {
                    $scope.showCatalogData($scope.catalog_name, $scope.catalog_title);
                    toastr.success("Запись успешно добавлена!");
                } else {
                    toastr.error("Произошла ошибка при добавлении, обратитесь к администратору, возможно у вас нет прав!");
                }
            })
        };

        $scope.cancelAdding = function () {
            $(".new_catalog_row").addClass("hidden");
        };

        $scope.init = function (value, modal) {
            if (value == null) $scope.catalog_data[modal.key][modal.field] = "";
        };

    }
);