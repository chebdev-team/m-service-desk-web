app.controller("createRequestCtrl", function ($scope, $http, $location, CatalogFields, Catalog, Request, Identity, User) {

    $scope.identity = Identity;

    if (Identity.currentUser == null) {
        $location.path("/");
        return;
    }

    $scope.current = {};
    $scope.current.limit_date = DateFormat.format.date(new Date(), "yyyy-MM-dd HH:mm");

    // get request types
    Catalog.query({catalog_name: "request_types"}).$promise.then(function (collection) {
        $scope.request_types = collection;
        $scope.current.type = collection[0];
    });

    // get request descriptions
    Catalog.query({catalog_name: "request_descriptions"}).$promise.then(function (collection) {
        $scope.request_descriptions = collection;
        $scope.current.description = {id: collection[0].id};
    });

    // get regions
    Catalog.query({catalog_name: "regions"}).$promise.then(function (collection) {
        $scope.regions = collection;
        $scope.current.region = {id: collection[0].id};
        $scope.filterClients();
    });

    // get engineers(role_id = 3)
    User.query({role_id: 3}).$promise.then(function (collection) {
        $scope.users = collection;
        $scope.users.push({id: 0, username: "Не назначен"});
        $scope.current.user = $scope.users[($scope.users.length - 1)];
    });

    $scope.current.free_description = undefined;

    // filter clients function by region id
    $scope.filterClients = function () {
        $http.get('/api/catalog/get_clients_by_region/' + $scope.current.region.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.clients = collection;
                $scope.current.client = {id: collection[0].id};
            } else {
                $scope.clients = [];
                $scope.current.client = {id: null};
            }
            $scope.filterLocations();
        });
    };
    // filter locations function by region id and client id
    $scope.filterLocations = function () {
        $http.get('/api/catalog/get_locations/' + $scope.current.region.id + '/' + $scope.current.client.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.locations = collection;
                $scope.current.location = {id: collection[0].id};
            } else {
                $scope.locations = [];
                $scope.current.location = {id: null};
            }
        });
    };

    $scope.createRequest = function () {
        var data = {
            type: $scope.current.type.id,
            client: $scope.current.client.id,
            region: $scope.current.region.id,
            location: $scope.current.location.id,
            limit_date: $scope.current.limit_date,
            free_description: $scope.current.free_description,
            description: $scope.current.description.id,
            client_request_id: $scope.current.client_request_id,
            user_id: $scope.current.user.id
        };

        if (data.limit_date.trim() == "" || data.free_description == null || data.location == null || data.client == null || !data.client_request_id) {
            toastr.error("Не все поля заполнены!");
            return;
        }

        Request.save(data).$promise.then(function (response) {
            if (response.success) {
                toastr.success("Заявка успешно добавлена!");
            } else {
                toastr.error("Произошла ошибка при создании, обратитесь к администратору, возможно у вас нет прав!");
            }
        });
    };

    $scope.changeToInt = function (value) {
        $scope.current.client_request_id = parseInt(value);
    };

    $scope.changeDate = function (value) {
        console.log("changed");
    };
});