app.controller("globalCtrl", function ($scope, Identity) {
    $scope.identity = Identity;
    $scope.$on('$routeChangeStart', function (next, current) {
        var path = current.$$route.originalPath;
        $(".menu_link").removeClass("active");
        $(".menu_link a").each(function () {
            if ($(this).attr("href") == path) {
                $(this).parent().addClass("active");
            }
        });
    });
});