app.controller("mainCtrl", function ($scope, Identity, $http, Request, Catalog) {
    $scope.identity = Identity;
    //TODO : remove in production
    window.myscope = $scope;

    if (Identity.currentUser == null) {
        return;
    }

    getLastRequests();
    $scope.empty_value = "Не заполнено";

    function getLastRequests() {
        // last created requests
        var fields = [
            "request_id",
            "request_type_title",
            "client_title",
            "registration_date"
        ];

        var params = {
            fields: fields.join(","),
            orderBy: "registration_date",
            order: "DESC",
            limit: 5
        };

        Request.query(params).$promise.then(function (collection) {
            $scope.last_created_requests = collection;
        });


        // last created requests
        fields = [
            "request_id",
            "request_type_title",
            "client_title",
            "finish_date"
        ];

        params = {
            fields: fields.join(","),
            orderBy: "finish_date",
            order: "DESC",
            limit: 5
        };

        Request.query(params).$promise.then(function (collection) {
            $scope.last_modified_requests = collection;
        });
    };

    $scope.openRequestModal = function (request_id) {
        $scope.current = {};
        Request.query({id: request_id}).$promise.then(function (response) {
            $scope.request_data = response[0];

            // get request types
            Catalog.query({catalog_name: "request_types"}).$promise.then(function (collection) {
                $scope.request_types = collection;
                $scope.current.type = {id: $scope.request_data.request_type_id};
            });

            // get request states
            Catalog.query({catalog_name: "request_states"}).$promise.then(function (collection) {
                $scope.request_states = collection;
                $scope.current.state = {id: $scope.request_data.state_id};
            });

            // get request descriptions
            Catalog.query({catalog_name: "request_descriptions"}).$promise.then(function (collection) {
                $scope.request_descriptions = collection;
                $scope.current.description = {id: $scope.request_data.description_id};
            });

            // get regions
            Catalog.query({catalog_name: "regions"}).$promise.then(function (collection) {
                $scope.regions = collection;
                $scope.current.region = {id: $scope.request_data.region_id};
                $scope.filterClients();
            });

            $scope.current.request_id = $scope.request_data.request_id;
            $scope.current.limit_date = $scope.request_data.limit_date;
            $scope.current.registration_date = $scope.request_data.registration_date;
            $scope.current.free_description = $scope.request_data.free_description;
            $scope.current.client_request_id = $scope.request_data.client_request_id;

        });
    };

    // filter clients function by region id
    $scope.filterClients = function (reset) {
        $http.get('/api/catalog/get_clients_by_region/' + $scope.current.region.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.clients = collection;
                $scope.current.client = reset ? collection[0] : {id: $scope.request_data.client_id};
            } else {
                $scope.clients = [];
                $scope.current.client = {id: null};
            }
            $scope.filterLocations(reset);
        });
    };

    // filter locations function by region id and client id
    $scope.filterLocations = function (reset) {
        $http.get('/api/catalog/get_locations/' + $scope.current.region.id + '/' + $scope.current.client.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.locations = collection;
                $scope.current.location = reset ? collection[0] : {id: $scope.request_data.location_id};
            } else {
                $scope.locations = [];
                $scope.current.location = {id: null};
            }
        });
    };

    $scope.saveRequest = function () {
        var data = {
            id: $scope.current.request_id,
            client: $scope.current.client.id,
            location: $scope.current.location.id,
            state: $scope.current.state.id,
            type: $scope.current.type.id,
            limit_date: $scope.current.limit_date,
            description: $scope.current.description.id,
            free_description: $scope.current.free_description,
            client_request_id: $scope.current.client_request_id
        };

        if (data.client == null || data.location == null || data.description == null || !data.limit_date.trim() || !data.free_description.trim() || !data.client_request_id) {
            toastr.error("Не все поля заполнены!!");
            return false;
        }

        Request.update({}, data).$promise.then(function (response) {
            if (response.success) {
                getLastRequests();
                toastr.success("Заявка успешно обновлена!");
            } else {
                toastr.error("Произошла ошибка при обновлении, обратитесь к администратору, возможно у вас нет прав!");
            }
        });
    };

    $scope.changeToInt = function (value) {
        $scope.current.client_request_id = parseInt(value);
    }


});