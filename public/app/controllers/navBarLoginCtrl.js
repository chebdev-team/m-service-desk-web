app.controller("navBarLoginCtrl", function ($scope, $http, Identity, Auth, $location, $route) {
    $scope.identity = Identity;
    $scope.signin = function (login, password) {
        Auth.authenticateUser(login, password).then(function (success) {
            if (success) {
                toastr.success("Привет " + Identity.currentUser.username + "!");
                //$location.path("/");
                $route.reload();
            } else {
                toastr.error("Неправильная комбинация логина и пароля!");
            }
        });
    };
    $scope.signout = function () {
        Auth.logoutUser().then(function () {
            $scope.username = "";
            $scope.password = "";
            toastr.success("Пока!")
            $location.path("/");
        });
    };
});