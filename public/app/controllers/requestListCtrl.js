app.controller("requestListCtrl", function ($scope, $http, Request, Catalog, Identity, $filter, $location, User) {

    $scope.identity = Identity;

    if (Identity.currentUser == null) {
        $location.path("/");
        return;
    }

    window.myscope = $scope;

    Request.query().$promise.then(function (collection) {
        $scope.requests = collection;
        $scope.sliced_requests = collection;

        $scope.total = collection.length;
        $scope.pageSize = 25;
        $scope.currentPage = 1;

        $scope.applyPagination($scope.currentPage);
    });

    $scope.request_data = {};
    $scope.predicate = "request_id";
    $scope.empty_value = "Не заполнено";
    $scope.current = {};
    $scope.filters = {};
    $scope.search = {};

    // get regions
    Catalog.query({catalog_name: "regions"}).$promise.then(function (collection) {
        $scope.filters.regions = collection;
    });

    // get clients
    Catalog.query({catalog_name: "clients"}).$promise.then(function (collection) {
        $scope.filters.clients = collection;
    });

    // get locations
    Catalog.query({catalog_name: "locations"}).$promise.then(function (collection) {
        $scope.filters.locations = collection;
    });

    // get request types
    Catalog.query({catalog_name: "request_types"}).$promise.then(function (collection) {
        $scope.filters.request_types = collection;
    });

    // get request states
    Catalog.query({catalog_name: "request_states"}).$promise.then(function (collection) {
        $scope.filters.request_states = collection;
    });

    $scope.openRequestModal = function (request_id) {

        Request.query({id: request_id}).$promise.then(function (response) {
            $scope.request_data = response[0];

            // get request types
            Catalog.query({catalog_name: "request_types"}).$promise.then(function (collection) {
                $scope.request_types = collection;
                $scope.current.type = {id: $scope.request_data.request_type_id};
            });

            // get request states
            Catalog.query({catalog_name: "request_states"}).$promise.then(function (collection) {
                $scope.request_states = collection;
                $scope.current.state = {id: $scope.request_data.state_id};
            });

            // get request descriptions
            Catalog.query({catalog_name: "request_descriptions"}).$promise.then(function (collection) {
                $scope.request_descriptions = collection;
                $scope.current.description = {id: $scope.request_data.description_id};
            });

            // get regions
            Catalog.query({catalog_name: "regions"}).$promise.then(function (collection) {
                $scope.regions = collection;
                $scope.current.region = {id: $scope.request_data.region_id};
                $scope.filterClients();
            });

            $scope.current.request_id = $scope.request_data.request_id;
            $scope.current.limit_date = $scope.request_data.limit_date;
            $scope.current.registration_date = $scope.request_data.registration_date;
            $scope.current.free_description = $scope.request_data.free_description;
            $scope.current.client_request_id = $scope.request_data.client_request_id;

        });
    };

    // filter clients function by region id
    $scope.filterClients = function (reset) {
        $http.get('/api/catalog/get_clients_by_region/' + $scope.current.region.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.clients = collection;
                $scope.current.client = reset ? collection[0] : {id: $scope.request_data.client_id};
            } else {
                $scope.clients = [];
                $scope.current.client = {id: null};
            }
            $scope.filterLocations(reset);
        });
    };

    // filter locations function by region id and client id
    $scope.filterLocations = function (reset) {
        $http.get('/api/catalog/get_locations/' + $scope.current.region.id + '/' + $scope.current.client.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                $scope.locations = collection;
                $scope.current.location = reset ? collection[0] : {id: $scope.request_data.location_id};
            } else {
                $scope.locations = [];
                $scope.current.location = {id: null};
            }
        });
    };

    $scope.saveRequest = function () {
        var data = {
            id: $scope.current.request_id,
            region: $scope.current.region.id,
            client: $scope.current.client.id,
            location: $scope.current.location.id,
            state: $scope.current.state.id,
            type: $scope.current.type.id,
            limit_date: $scope.current.limit_date,
            description: $scope.current.description.id,
            free_description: $scope.current.free_description,
            client_request_id: $scope.current.client_request_id
        };

        if (data.client == null || data.location == null || data.description == null || !data.limit_date.trim() || !data.free_description.trim() || !data.client_request_id) {
            toastr.error("Не все поля заполнены!!");
            return false;
        }

        Request.update({}, data).$promise.then(function (response) {
            if (response.success) {
                Request.query().$promise.then(function (collection) {
                    $scope.requests = collection;
                    $scope.applyPagination();
                });
                toastr.success("Заявка успешно обновлена!");
            } else {
                toastr.error("Произошла ошибка при обновлении, обратитесь к администратору, возможно у вас нет прав!");
            }
        });
    };

    $scope.getCurrentEngineer = function (request_id) {
        Request.query({id: request_id, fields: "assigned_to, username"}).$promise.then(function (collection) {
            $scope.current.assigned_to = collection[0].username;
            $scope.current.request_id = request_id;
        });
        User.query({role_id: "3"}).$promise.then(function (collection) {
            $scope.current.engineer_list = collection;
            $scope.current.reassigned_to = collection[0];
        });
    };

    $scope.applyNewEngineer = function () {
        Request.update({id: $scope.current.request_id, assigned_to: $scope.current.reassigned_to.id}).$promise.then(function (response) {
            if (response.success) {
                toastr.success("Инженер успешно назначен!");
            } else {
                toastr.error("Произошла ошибка, обратитесь к администратору, возможно у вас нет прав!");
            }
        });
    };

    $scope.pageChanged = function (page) {
        $scope.applyPagination(page);
    };

    $scope.applyPagination = function (page, filter) {
        var requests = $scope.requests;

        if (page == null) page = $scope.currentPage;
        if (filter) {
            requests = $scope.sliced_requests;
            $scope.total = requests.length;
        }

        var start = $scope.pageSize * page - $scope.pageSize,
            end = start + $scope.pageSize;

        $scope.currentPage = page;
        $scope.sliced_requests = requests.slice(start, end);

    };

    $scope.filterApplied = function (query) {
        $scope.sliced_requests = $filter('filter')($scope.requests, query);
        $scope.applyPagination(1, true);
    };

    $scope.changeToInt = function (value) {
        $scope.current.client_request_id = parseInt(value);
    }

});