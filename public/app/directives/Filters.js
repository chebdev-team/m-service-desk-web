app.filter("myFilter", function () {
    return function (input, value) {
        var size = _.keys(value).length;
        var _t = 0;
        _.values(value).forEach(function (item) {
            if (!item) _t++;
        });
        if (_t == size) return input;
        var result = [], add;
        if (input != null) {
            for (var i = 0; i < input.length; i++) {
                add = false;
                for (var key in value) {
                    if (!value.hasOwnProperty(key)) continue;
                    if (input[i][key] !== undefined) {
                        if (value[key] === null) value[key] = "";
                        if (input[i][key].indexOf(value[key]) != -1) {
                            add = true;
                        } else {
                            add = false;
                            break;
                        }
                    }
                }
                if (add) result.push(input[i]);
            }
        }
        return result;
    }
});