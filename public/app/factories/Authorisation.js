app.factory("Identity", function ($window) {
    var currentUser;
    if (!!$window.bootstrapedUser) {
        currentUser = $window.bootstrapedUser;
    }
    return {
        currentUser: currentUser,
        isAuthenticated: function () {
            return !!this.currentUser;
        }
    }
});

app.factory("Auth", function ($http, Identity, $q) {
    return {
        authenticateUser: function (login, password) {
            var dfd = $q.defer();
            $http.post("/api/login", {login: login, password: password}).then(function (response) {
                if (response.data.success) {
                    Identity.currentUser = response.data.user;
                    dfd.resolve(true);
                } else {
                    dfd.resolve(false);
                }
            });
            return dfd.promise;
        },

        logoutUser: function () {
            var dfd = $q.defer();
            $http.get("/api/logout").then(function () {
                Identity.currentUser = undefined;
                dfd.resolve();
            });
            return dfd.promise;
        }
    }
});