app.factory("RequestModal", function ($http, Request, Catalog) {

    var scope = {};

    scope.init = function (request_data) {
        scope.request_data = request_data;
    };

    scope.openRequestModal = function (request_id) {
        scope.current = {};
        Request.query({id: request_id}).$promise.then(function (response) {
            scope.request_data = response[0];

            // get request types
            Catalog.query({catalog_name: "request_types"}).$promise.then(function (collection) {
                scope.request_types = collection;
                scope.current.type = {id: scope.request_data.request_type_id};
            });

            // get request states
            Catalog.query({catalog_name: "request_states"}).$promise.then(function (collection) {
                scope.request_states = collection;
                scope.current.state = {id: scope.request_data.state_id};
            });

            // get request descriptions
            Catalog.query({catalog_name: "request_descriptions"}).$promise.then(function (collection) {
                scope.request_descriptions = collection;
                scope.current.description = {id: scope.request_data.description_id};
            });

            // get regions
            Catalog.query({catalog_name: "regions"}).$promise.then(function (collection) {
                scope.regions = collection;
                scope.current.region = {id: scope.request_data.region_id};
                filterClients();
            });

            scope.current.request_id = scope.request_data.request_id;
            scope.current.limit_date = scope.request_data.limit_date;
            scope.current.registration_date = scope.request_data.registration_date;
            scope.current.free_description = scope.request_data.free_description;
        });

        //console.log(scope.current);

        return scope;
    };

    // filter clients function by region id
    function filterClients(reset) {
        $http.get('/api/catalog/get_clients_by_region/' + scope.current.region.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                scope.clients = collection;
                scope.current.client = reset ? collection[0] : {id: scope.request_data.client_id};
            } else {
                scope.clients = [];
                scope.current.client = {id: null};
            }
            filterLocations(reset);
        });
    }

    // filter locations function by region id and client id
    function filterLocations(reset) {
        $http.get('/api/catalog/get_locations/' + scope.current.region.id + '/' + scope.current.client.id).success(function (collection) {
            if (_.isEmpty(collection) == false) {
                scope.locations = collection;
                scope.current.location = reset ? collection[0] : {id: scope.request_data.location_id};
            } else {
                scope.locations = [];
                scope.current.location = {id: null};
            }
        });
    }

});