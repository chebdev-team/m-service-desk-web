app.factory("Request", function ($resource) {
    var RequestResourse = $resource("/api/requests/?", {}, {
        update: {method: "PUT", isArray: false}
    });
    return RequestResourse;
});

app.factory("CatalogList", function ($resource) {
    var CatalogListResource = $resource("/api/get_catalog_list");
    return CatalogListResource;
});

app.factory("CatalogFields", function ($resource) {
    var CatalogFieldsResource = $resource("/api/catalog_fields/:catalog_name", {catalog_name: "@catalog_name"});
    return CatalogFieldsResource;
});

app.factory("Catalog", function ($resource) {
    var CatalogResource = $resource("/api/catalog/:catalog_name/:id", {catalog_name: "@catalog_name", id: "@id"}, {
        update: {method: "PUT", isArray: false}
    });
    return CatalogResource;
});

app.factory("User", function ($resource) {
    var UserResource = $resource("/api/user/?", {}, {
        update: {method: "PUT", isArray: false}
    });
    return UserResource;
});
