<?php

class Database {

	public $host = MS_HOST;
	public $user = MS_USER;
	public $password = MS_PASSWORD;
	public $database = MS_DATABASE;
	public $dbh;

	function __construct() {
		$this->dbh = new PDO(
			"mysql:host={$this->host};dbname={$this->database}",
			$this->user,
			$this->password,
			array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			) );
	}

	function execute( $sql ) {
		if ( ! isset( $_SESSION["login"] ) ) {
			return false;
		} else {
			$sth = $this->dbh->prepare( $sql );
			$sth->execute();
//			return
		}

		return true;
	}

	function get_request( $id = null ) {
		if ( ! $id ) {
			throw new Exception( "Need to specify request ID" );
		}
		$where = "WHERE r.id = '{$id}'";
		$sql   = "SELECT r.id, rt.id as type, c.id as client, l.id as location, reg.id as region,
							r.registration_date, r.limit_date, rs.id as state, r.free_description, r.description
					FROM requests r
						LEFT JOIN request_types rt ON rt.id = r.type
						LEFT JOIN client_location cl ON cl.id = r.client_location_id
						LEFT JOIN locations l ON l.id = cl.location_id
						LEFT JOIN clients c ON c.id = cl.client_id
						LEFT JOIN regions reg ON reg.id = l.region_id
						LEFT JOIN request_states rs ON rs.id = r.state
					{$where}";
		$sth   = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		return $result;
	}

	function get_requests( $params = null ) {
		if ( ! $params ) {
			throw new Exception( "No parameters passed!!" );
		}

		$fields = "*";
		if ( isset( $params["fields"] ) ) {
			$fields = $params["fields"];
		}
		$conditions = "WHERE 1";
		$and        = "";
		$orderBy    = "";
		$order      = "";
		$limit      = "";

		if ( isset( $params["id"] ) ) {
			$and .= " AND request_id = '{$params["id"]}'";
		}

		if ( isset( $params["region_id"] ) && $params["region_id"] != "-1" ) {
			$and .= " AND region_id = '{$params["region_id"]}'";
		}
		if ( isset( $params["user_id"] ) ) {
			$and .= " AND assigned_to = '{$params["user_id"]}'";
		}
		if ( isset( $params["state_id"] ) ) {
			$and .= "AND state_id IN ({$params["state_id"]})";
		}

		if ( isset( $params["orderBy"] ) ) {
			$orderBy = "ORDER BY {$params["orderBy"]}";
			if ( isset( $params["order"] ) ) {
				$order = $params["order"];
			}
		}
		if ( isset( $params["limit"] ) ) {
			$limit = "LIMIT {$params["limit"]}";
		}
		$conditions .= " {$and} {$orderBy} {$order} {$limit}";
		$sql = "SELECT {$fields} FROM requests_view {$conditions}";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		return $result;
	}

	function android_get_requests_by_region( $region_id = null ) {
		if ( ! $region_id ) {
			throw new Exception( "Need to specify region ID" );
		}
		$and = "AND l.region_id = {$region_id}";
		if ( $region_id == "-1" ) {
			$and = "";
		}
		$sql = "SELECT r.id, c.title as client, l.title as client_address, r.description, CONCAT(u.first_name, ' ', u.last_name) as assigned
					FROM requests r
					LEFT JOIN client_location cl ON cl.id = r.client_location_id
					LEFT JOIN clients c ON c.id = cl.client_id
					LEFT JOIN locations l ON l.id = cl.location_id
					LEFT JOIN users u ON u.id = r.assigned_to
				WHERE r.state < 3 {$and}";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		return $result;
	}

	function android_get_requests_by_user( $user_id ) {
		if ( ! $user_id ) {
			throw new Exception( "Need to specify user ID" );
		}
		$sql = "SELECT r.id, c.title as client, l.title as client_address, r.description, CONCAT(u.first_name, ' ', u.last_name) as username
						FROM requests r
						LEFT JOIN client_location cl ON cl.id = r.client_location_id
						LEFT JOIN clients c ON c.id = cl.client_id
						LEFT JOIN locations l ON l.id = cl.location_id
						LEFT JOIN users u ON u.id = r.assigned_to
					WHERE r.assigned_to = '{$user_id}' AND r.state < 3";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		return $result;
	}

	function android_take_request( $id = null, $data = null ) {
		if ( ! $id || ! $data || ! isset( $data["assigned_to"] ) || ! isset( $data["state"] ) ) {
			throw new Exception( "Need to specify request ID and data array" );
		}

		$sql = "UPDATE requests SET state = '{$data["state"]}',
									assigned_to =
									CASE    WHEN assigned_to IS NULL THEN '{$data["assigned_to"]}'
											ELSE assigned_to
									END
									WHERE id = '{$id}' LIMIT 1";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$count = $sth->rowCount();
		if ( $count == 0 ) {
			return false;
		}

		return true;
	}

	function android_update_request( $id = null, $data = null ) {
		if ( ! $id || ! $data ) {
			throw new Exception( "Need to specify request ID and data array" );
		}
		if ( ! isset( $data["assigned_to"] ) ) {
			throw new Exception( "Need to specify user id" );
		}
		$user_id = $data["assigned_to"];
		unset( $data["assigned_to"] );

		$sql = "UPDATE requests SET ";
		foreach ( $data as $key => $value ) {
			$sql .= $key . " = '{$value}', ";
		}
		$sql = substr( $sql, 0, - 2 ) . " WHERE id = '{$id}' AND assigned_to = '{$user_id}' LIMIT 1";
		$sth = $this->dbh->prepare( $sql );

		if ( ! $sth->execute() ) {
			return $sth->errorInfo();
		}

		return true;
	}

	function android_get_requests( $id = null ) {
		$where = "";
		if ( $id ) {
			$where = "WHERE r.id = '{$id}'";
		}
		$sql = "SELECT r.id as id, rt.title as type, c.title as client, l.title as address,
		r.registration_date, r.limit_date, rs.id as state_id, rs.title as state_title, r.description
					FROM requests r
						LEFT JOIN request_types rt ON rt.id = r.type
						LEFT JOIN client_location cl ON cl.id = r.client_location_id
						LEFT JOIN locations l ON l.id = cl.location_id
						LEFT JOIN clients c ON c.id = cl.client_id
						LEFT JOIN regions reg ON reg.id = l.region_id
						LEFT JOIN request_states rs ON rs.id = r.state
					{$where}";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetch( PDO::FETCH_ASSOC );

		return $result;
	}

	function delete_request( $id = null ) {
		if ( ! $id ) {
			throw new Exception( "Need to specify request ID" );
		}
		$sql = "DELETE FROM requests WHERE id = '{$id}' LIMIT 1";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
	}

	function update_request( $data = null ) {
		if ( ! $data || ! isset( $data["id"] ) ) {
			throw new Exception( "Need id and data to update request" );
		}

		$and = "";
		if ( isset( $data["client"] ) && isset( $data["location"] ) ) {
			$client_location_id = $this->get_client_location_id( $data["client"], $data["location"] );
			if ( ! $client_location_id ) {
				return false;
			}
			$and .= "client_location_id = '{$client_location_id}', ";
		}
		unset ( $data["region"] );
		unset ( $data["client"] );
		unset ( $data["location"] );

		$sql = "UPDATE requests SET ";
		foreach ( $data as $key => $value ) {
			$sql .= "{$key} = '{$value}', ";
		}
		$sql .= $and;
		$sql = substr( $sql, 0, - 2 ) . " WHERE id = '{$data["id"]}' LIMIT 1";
		$sth = $this->dbh->prepare( $sql );
		if ( ! $sth->execute() ) {
			return false;
		}

		return true;
	}

	function add_request( $data = null ) {
		if ( ! $data ) {
			throw new Exception( "No data passed to create request" );
		}
		$client_location_id = $this->get_client_location_id( $data["client"], $data["location"] );
		if ( ! $client_location_id ) {
			return false;
		}
		if ( isset( $data["user_id"] ) && $data["user_id"] == "0" ) {
			$data["user_id"] = "NULL";
		}

		$sql = "INSERT INTO requests SET    type = '{$data["type"]}',
											state = 1,
											client_location_id = '{$client_location_id}',
											registration_date = NOW(),
											limit_date = '{$data["limit_date"]}',
											free_description = '{$data["free_description"]}',
											description = '{$data["description"]}',
											client_request_id = '{$data["client_request_id"]}',
											assigned_to = {$data["user_id"]}";
		$sth = $this->dbh->prepare( $sql );
		if ( ! $sth->execute() ) {
			return false;
		}

		return true;
	}

	function get_client_location_id( $client_id, $address_id ) {
		$sql = "SELECT id FROM client_location WHERE client_id = {$client_id} AND location_id = {$address_id} LIMIT 1";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetch( PDO::FETCH_ASSOC );
		if ( ! $result ) {
			return false;
		}

		return $result["id"];
	}

	function create_request( $data = null ) {
		if ( ! $data ) {
			throw new Exception( "No data passed to create request" );
		}
	}

	function close_connection() {
		$this->dbh = null;
	}
}