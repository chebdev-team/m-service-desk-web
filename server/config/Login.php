<?php

class Login {

	public $dbh;

	public function __construct( PDO $dbh ) {
		if ( ! $dbh ) {
			throw new Exception( "No database handler passed" );
		}
		session_start();
		$this->dbh = $dbh;
	}

	public function SignIn( $login, $password ) {
		$login    = trim( $login );
		$password = md5( trim( $password ) );
		$sql      = "SELECT id as user_id, region_id, CONCAT(first_name, ' ', last_name) as username
								FROM users WHERE login = '{$login}' AND password = '{$password}'
								LIMIT 1";
		$sth      = $this->dbh->prepare( $sql );

		if ( $sth->execute() && $result = $sth->fetch( PDO::FETCH_ASSOC ) ) {
			$_SESSION["login"] = $result;

			return $result;
		}

		return false;
	}

	public function SignOut() {
		unset( $_SESSION["login"] );
		session_destroy();
	}
} 