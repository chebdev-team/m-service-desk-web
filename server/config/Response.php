<?php

require_once( $_SERVER["DOCUMENT_ROOT"] . "/server/helpers/ArrayToXML.php" );

class Response {
	public static function Send( $data, $format = "json" ) {
		switch ( $format ) {
			case "xml":
				self::SendXML( $data );
				break;
			case "json":
			default:
				self::SendJSON( $data );
				break;
		}
	}

	public static function SendStatus( $status ) {
		$message = "";
		switch ( $status ) {
			case "404":
				$message = "Not Found";
				break;
			case "403":
				$message = "Forbidden";
				break;
			case "200":
				$message = "OK";
				break;
			default:
				break;
		}
		header( "HTTP/1.0 {$status} {$message}" );
		exit();
	}

	public static function SendText( $text ) {
		header( "HTTP/1.0 200 OK" );
		echo $text;
		exit();
	}

	public static function Send404() {
		header( 'HTTP/1.0 404 Not Found' );
		echo "<h1>404 Not Found</h1>";
		echo "The page that you have requested could not be found.";
		exit();
	}

	public static function Send403() {
		header( 'HTTP/1.0 403 Forbidden' );
		echo "<h1>403 Forbidden</h1>";
		exit();
	}

	public static function SendJSON( $data ) {
		header( 'Content-Type: application/json; charset=utf-8' );
		echo json_encode( $data );
		exit();
	}

	public static function SendXML( $data ) {
		$result = ArrayToXML::toXml( $data, "data", "request", null );
		header( 'Content-Type: application/xml; charset=utf-8' );
		echo $result;
		exit();
	}
} 