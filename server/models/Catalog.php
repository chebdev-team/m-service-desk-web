<?php

class Catalog {

	public $current_catalog;
	public $dbh;

	public function __construct( PDO $dbh ) {
		if ( ! $dbh ) {
			throw new Exception( "No database handler  passed" );
		}
		$this->dbh = $dbh;
	}

	public function get_catalog_fields( $catalog_name = null ) {
		if ( ! $catalog_name ) {
			throw new Exception( "No catalog field passed" );
		}
		$sql = "DESCRIBE {$catalog_name}";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = [ ];
		foreach ( $sth->fetchAll( PDO::FETCH_ASSOC ) as $value ) {
			$result[] = $value["Field"];
		}

		return $result;
	}

	public function get_catalog_data( $catalog_name = null ) {
		if ( ! $catalog_name ) {
			throw new Exception( "No catalog name passed" );
		}
		$sql = "SELECT * FROM {$catalog_name} WHERE id > 0";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();

		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		if ( $result && $catalog_name == "users" ) {
			foreach ( $result as &$value ) {
				if ( isset( $value["password"] ) ) {
					$value["password"] = "";
				}
			}
		}

		return $result;
	}

	public function delete_row_from_catalog( $catalog_name = null, $row_id = null ) {
		if ( ! $catalog_name || ! $row_id ) {
			throw new Exception( "No ID or catalog name passed" );
		}
		$sql = "DELETE FROM {$catalog_name} WHERE id = '{$row_id}' LIMIT 1";
		$sth = $this->dbh->prepare( $sql );
		if ( ! $sth->execute() ) {
			return false;
		}

		return true;
	}

	public function update_catalog_row( $catalog_name = null, $row_id = null, $data = null ) {
		if ( ! $catalog_name || ! $row_id || ! $data ) {
			throw new Exception( "No ID or catalog name passed" );
		}
		$sql = "UPDATE {$catalog_name} SET ";
		foreach ( $data as $key => $value ) {
			if ( $key == "password" ) {
				$value = md5( $value );
			}
			if ( $value == - 1 ) {
				return false;
			}
			$sql .= "{$key} = '{$value}', ";
		}
		$sql = substr( $sql, 0, - 2 ) . " WHERE id = '{$row_id}'";
		$sth = $this->dbh->prepare( $sql );
		if ( ! $sth->execute() ) {
			return false;
		}

		return true;
	}

	public function insert_catalog_row( $catalog_name = null, $data = null ) {
		if ( ! $catalog_name ) {
			throw new Exception( "No data or catalog name passed" );
		}
		$sql = "INSERT INTO {$catalog_name}(" . implode( ",", array_keys( $data ) ) . ") VALUES ('" . implode( "','", array_values( $data ) ) . "')";
		$sth = $this->dbh->prepare( $sql );
		if ( ! $sth->execute() || ! $data ) {
			return false;
		}

		return true;
	}

	public function get_clients_by_region( $region_id = null ) {
		if ( ! $region_id ) {
			throw new Exception( "No region ID passed" );
		}
		$sql = "SELECT c.id, c.title FROM client_location cl
					LEFT JOIN locations l ON l.id = cl.location_id
					LEFT JOIN clients c ON c.id = cl.client_id
				WHERE l.region_id = '{$region_id}'";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();

		return $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	public function get_locations_by_region( $region_id = null, $client_id = null ) {
		if ( ! $region_id || ! $client_id ) {
			throw new Exception( "No region ID or client ID passed" );
		}
		$sql = "SELECT l.id, l.title as address FROM client_location cl
						LEFT JOIN locations l ON l.id = cl.location_id
					WHERE l.region_id = '{$region_id}' AND cl.client_id = '{$client_id}'";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();

		return $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	public function get_catalogs() {
		return [
			[
				"clients"                => [ "title" => "Клиенты", "dependencies" => [ ] ],
				"locations"              => [ "title" => "Адреса", "dependencies" => [ "regions" => "region_id" ] ],
				"client_location"        => [ "title" => "Клиент-Адрес", "dependencies" => [ "clients" => "client_id", "locations" => "location_id" ] ],
				"regions"                => [ "title" => "Регионы", "dependencies" => [ ] ],
				"request_types"          => [ "title" => "Типы заявок", "dependencies" => [ ] ],
				"request_states"         => [ "title" => "Статусы заявок", "dependencies" => [ ] ],
				"request_finished_state" => [ "title" => "Статусы объекта после закрытия заявки", "dependencies" => [ ] ],
				"users"                  => [ "title" => "Пользователи", "dependencies" => [ "roles" => "role_id", "regions" => "region_id" ], "hide" => [ "password" => "true" ] ],
				"roles"                  => [ "title" => "Роли", "dependencies" => [ ] ],
				"request_descriptions"   => [ "title" => "Описания заявок", "dependencies" => [ ] ]
			]
		];
	}
}