<?php

class User {

	public $dbh;

	public function __construct( PDO $dbh ) {
		if ( ! $dbh ) {
			throw new Exception( "No database handler  passed" );
		}
		$this->dbh = $dbh;
	}

	public function get_users( $params ) {
		$and = "";
		if ( isset( $params["role_id"] ) ) {
			$and .= " AND role_id IN({$params["role_id"]})";
		}
		if ( isset( $params["id"] ) ) {
			$and .= " AND id = '{$params["id"]}'";
		}
		$sql = "SELECT id, CONCAT(first_name, ' ', last_name) as username, role_id
					FROM users WHERE 1 {$and}";
		$sth = $this->dbh->prepare( $sql );
		$sth->execute();
		$result = $sth->fetchAll( PDO::FETCH_ASSOC );

		return $result;

	}

}