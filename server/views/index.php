<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="/public/favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link rel="stylesheet" href="/public/vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/public/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="/public/vendor/toastr/toastr.css">
	<link rel="stylesheet" href="/public/css/style.css">
	<title>M-Service Desk</title>
</head>
<body ng-app="app" ng-controller="globalCtrl">
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header"><a href="/" class="navbar-brand">MService-Desk</a></div>
		<div class="navbar-collapse collapse">
			<div ng-include="'public/app/partials/common/top-menu.html'"></div>
			<div ng-include="'public/app/partials/account/navbar-login.html'"></div>
		</div>
	</div>
</div>
<section class="modal-content">
	<div ng-view></div>
</section>
<script type="text/javascript" src="/public/vendor/jquery/dist/jquery.js"></script>
<script type="text/javascript" src="/public/vendor/jquery-dateFormat/dist/dateFormat.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
<script type="text/javascript" src="/public/vendor/angular-resource/angular-resource.js"></script>
<script type="text/javascript" src="/public/vendor/angular-route/angular-route.js"></script>
<script type="text/javascript" src="/public/vendor/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/public/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/public/vendor/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/public/vendor/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/public/vendor/bootbox/bootbox.js"></script>
<script type="text/javascript" src="/public/vendor/underscore/underscore-min.js"></script>
<script type="text/javascript" src="/public/app/app.js"></script>
<script type="text/javascript" src="/public/app/controllers/mainCtrl.js"></script>
<script type="text/javascript" src="/public/app/controllers/createRequestCtrl.js"></script>
<script type="text/javascript" src="/public/app/controllers/requestListCtrl.js"></script>
<script type="text/javascript" src="/public/app/factories/Resources.js"></script>
<script type="text/javascript" src="/public/app/controllers/catalogListCtrl.js"></script>
<script type="text/javascript" src="/public/app/controllers/navBarLoginCtrl.js"></script>
<script type="text/javascript" src="/public/app/factories/Authorisation.js"></script>
<script type="text/javascript" src="/public/app/controllers/globalCtrl.js"></script>
<script type="text/javascript" src="/public/app/directives/Paging.js"></script>
<script type="text/javascript" src="/public/app/factories/RequestModal.js"></script>
<script type="text/javascript" src="/public/app/directives/Filters.js"></script>

<?php
if ( isset( $_SESSION["login"] ) ) {
	require_once( $_SERVER["DOCUMENT_ROOT"] . "/server/includes/bootstrapedUser.php" );
}
?>

</body>